# alpine-git & wget & https

该镜像基于alpine，安装了git、wget，并开启了对https下载的支持。

### pull down the image

``docker pull hedaqing/docker-alpine-git``

### run the image

``docker run -itd --name=git -v <folder-path-on-host>:/home/root/ --net host hedaqing/docker-alpine-git``

use ``--net host``,then you can visite website.

### into container

`` docker exec -it git /bin/sh``
